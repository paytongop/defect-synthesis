include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include <stdio.h>
#include <vector>
#include <fstream> 
#include <iostream> 

using namespace cv;

#define S_MAX 255.0
#define L_MAX 255.0
#define SL_MAX 255.0
#define DEF_RATIO 0.1
#define DEF_TYPE_NUM 6

Scalar ScalarHSV2BGR(uchar H, uchar S, uchar V) {
	Mat rgb;
	Mat hsv(1, 1, CV_8UC3, Scalar(H, S, V));
	cvtColor(hsv, rgb, COLOR_HSV2BGR);
	return Scalar(rgb.data[0], rgb.data[1], rgb.data[2]);
}

bool polynomial_curve_fit(std::vector<Point>& key_point, int n, Mat& A)
{
	//Number of key points
	int N = key_point.size();

	Mat X = Mat::zeros(n + 1, n + 1, CV_64FC1);
	for (int i = 0; i < n + 1; i++)
	{
		for (int j = 0; j < n + 1; j++)
		{
			for (int k = 0; k < N; k++)
			{
				X.at<double>(i, j) = X.at<double>(i, j) +
					std::pow(key_point[k].x, i + j);
			}
		}
	}

	Mat Y = Mat::zeros(n + 1, 1, CV_64FC1);
	for (int i = 0; i < n + 1; i++)
	{
		for (int k = 0; k < N; k++)
		{
			Y.at<double>(i, 0) = Y.at<double>(i, 0) +
				std::pow(key_point[k].x, i) * key_point[k].y;
		}
	}

	A = Mat::zeros(n + 1, 1, CV_64FC1);

	solve(X, Y, A, cv::DECOMP_LU);
	return true;
}

int main(int argc, char *argv[])
{
	std::ofstream fout("Label.txt", std::ofstream::out);
	std::ifstream fin;
	char wndname[] = "Preview";
	const int NUMBER = 100;
	const int DELAY = 5;
	int lineType = LINE_AA; // change it to LINE_8 to see non-antialiased graphics
	int lineWidth = 1; // change it to LINE_8 to see non-antialiased graphics
	int i, j, width = 1024, height = 768;
	int x1 = -width / 2, x2 = width * 3 / 2, y1 = -height / 2, y2 = height * 3 / 2;
	//RNG rng(0xFFFFFFFF);
	RNG rng(getTickCount());

	Mat image = Mat::zeros(height, width, CV_8UC3);
	imshow(wndname, image);
	waitKey(DELAY);
	Mat background;// = Mat::zeros(height, width, CV_8UC3);;
	image.copyTo(background);
	Mat syn = Mat::zeros(height, width, CV_8UC3);
	double alpha = 0.5;
	int blue = 0;
	Point pt;
	//int R = 0, G = 0, B = 0, Y = 0, U = 0, V = 0;
	double H = 0, L = 0, S = 0;
	double SL_inc = 0;
	double SL_min = 0;
	double alpha_inc = 0;
	double alpha_bkg = 0;
	//int L_min = 0;
	int render = 0;
	int defect_type = 0;
	int defect_level = 1;
	int pic_num = 2048;
	double def_x = 0;
	double def_y = 0;
	double def_xran = width*DEF_RATIO;
	double def_yran = height*DEF_RATIO;
	int box_x = 0;
	int box_y = 0;
	int box_w = 0;
	int box_h = 0;
	char imgfname[16];
	String fline;
	int instart = 0;
	int inend = 0;
	int set_width = -1;
	int set_height = -1;
	int set_picnum = -1;
	int set_transavg = -1;
	int set_transvar = -1;
	int set_defecttype = -1;
	int set_defectlevel = -1;

	if (argc > 2)
	{
		fin.open(argv[1], std::ifstream::in);
	}
	else
	{
		fin.open("Synthesis_Parameters.txt", std::ifstream::in);
	}

	if(fin.is_open())
	{
		while (fin.good())
		{
			getline(fin, fline);
			
			if (fline.find("Img_Width") != String::npos)
			{
				instart=fline.find_first_of("1234567890");
				inend = fline.find_first_not_of("1234567890", instart);
				set_width = std::stoi(fline.substr(instart, inend), nullptr);
			}
			if (fline.find("Img_Height") != String::npos)
			{
				instart = fline.find_first_of("1234567890");
				inend = fline.find_first_not_of("1234567890", instart);
				set_height = std::stoi(fline.substr(instart, inend), nullptr);
			}
			if (fline.find("Img_Number") != String::npos)
			{
				instart = fline.find_first_of("1234567890");
				inend = fline.find_first_not_of("1234567890", instart);
				set_picnum = std::stoi(fline.substr(instart, inend), nullptr);
			}
			if (fline.find("Transparency_Avg") != String::npos)
			{
				instart = fline.find_first_of("1234567890");
				inend = fline.find_first_not_of("1234567890", instart);
				set_transavg = std::stoi(fline.substr(instart, inend), nullptr);
			}
			if(fline.find("Transparency_Var") != String::npos)
			{
				instart = fline.find_first_of("1234567890");
				inend = fline.find_first_not_of("1234567890", instart);
				set_transvar = std::stoi(fline.substr(instart, inend), nullptr);
			}
			if (fline.find("Defect_Type") != String::npos)
			{
				instart = fline.find_first_of("1234567890");
				inend = fline.find_first_not_of("1234567890", instart);
				set_defecttype = std::stoi(fline.substr(instart, inend), nullptr);
			}
			if (fline.find("Defect_Level") != String::npos)
			{
				instart = fline.find_first_of("1234567890");
				inend = fline.find_first_not_of("1234567890", instart);
				set_defectlevel = std::stoi(fline.substr(instart, inend), nullptr);
			}
		}
	}
	//system("pause");
	if (set_width > 0)
		width = set_width;
	if (set_height > 0)
		height = set_height;
	if (set_picnum > 0)
		pic_num=set_picnum;
	fout << "File_name, Defect_type, BoundBox_X, BoundBox_Y, BoundBox_W, BoundBox_H" << std::endl;
	for (j = 0; j < pic_num; j++)
	{
		image = Mat::zeros(height, width, CV_8UC3);
		syn = Mat::zeros(height, width, CV_8UC3);
		H = 0;
		L = 0;
		S = 0;
		render = rng.uniform(0, 2);
		//render = 6;
		if (set_defecttype >= 0)
			defect_type = set_defecttype;
		else
			defect_type = rng.uniform(0, DEF_TYPE_NUM);
		SL_min = 0;
		alpha_bkg = 1.0;
		if (render == 0)
			SL_inc = 2 * (SL_MAX - SL_min) / height;
		else if (render == 1)
			SL_inc = 4 * (SL_MAX - SL_min) / height;
		else if (render == 2)
			SL_inc = (SL_MAX - SL_min) / height;
		else if (render == 3)
			SL_inc = (SL_MAX - SL_min) / height;
		else if (render == 4)
			SL_inc = 2 * (SL_MAX - SL_min) / height;
		else if (render == 5)
			SL_inc = (SL_MAX - SL_min) / height;
		else if (render == 6)
			SL_inc = (SL_MAX - SL_min) / height;
		else if (render == 8)
			alpha_inc = 1.0 / height;
		
		for (i = 0; i < height; i += 1)
		{
			Point pt1, pt2;
			pt1.x = 0;
			pt1.y = i;
			pt2.x = width - 1;
			pt2.y = i;
			if (render < 5)
			{
				Scalar c = ScalarHSV2BGR(300, S, L);
				line(image, pt1, pt2, c, 1, 0);
				if (render == 0)
				{
					//L = 128;
					//S += SL_inc;
					//if (S > 255)
					//{
					//	//if (S > 255)
					//	//{
					//	//	L = 255;
					//	//	S = 255;
					//	//}
					//	S = 255;
					//}
					//		
					////S += 10*SL_inc;
					//if (S > 255)
					//{ 
					//	L += 2;//SL_inc;
					//	if (L > 255)
					//	{
					//		L = 255;
					//		S = 255;
					//	}
					//	else
					//		S = 0;
					//}

					if (i % 2 == 0)
					{
						S += SL_inc;
						if (S > 255)
							S = 255;
					}
					else
					{
						L += SL_inc;
						if (L > 255)
							L = 255;
					}
				}
				else if(render==1)
				{
					if (i % 2 == 0)
					{
						S += SL_inc;
						if (S > 254)
							SL_inc = -SL_inc;
					}
					else
					{
						L += SL_inc;
						if (L > 254)
							SL_inc = -SL_inc;
					}
				}
				else if(render==2)
				{
					S += SL_inc;
					if (S > 254)
						SL_inc = -SL_inc;
					L = 128;
				}
				else if (render == 3)
				{
					L += SL_inc;
					if (L > 254)
						SL_inc = -SL_inc;
					S = 128;
				}
				else if (render == 4)
				{
					L += SL_inc;
					if (L > 254)
					{
						L = 254;
						S += SL_inc;
						if (S > 254)
							S = 254;
					}
				}
			}
			else if (render == 5)
			{
				for (int x = 0; x < width; x++)
				{
					L = 128;
					//S = 254-(i*SL_inc + x*(SL_MAX- i*SL_inc)/width);
					S = i*SL_inc + x*(SL_MAX - i*SL_inc) / width;
					if (S > 254)
						S = 254;
					Scalar c = ScalarHSV2BGR(300, S, L);
					Vec3b val;
					val[0] = (int)c[0];
					val[1] = (int)c[1];
					val[2] = (int)c[2];
					//val[0] = 0; val[1] = (i * 255) / height; val[2] = (height - i) * 255 / height;

					image.at<Vec3b>(i, x) = val;
				}
					
			}
			else if (render == 6)
			{
				for (int x = 0; x < width; x++)
				{
					S = 254;
					L = 254 - (i*SL_inc + x*(SL_MAX - i*SL_inc) / width);
					//L = i*SL_inc + x*(SL_MAX - i*SL_inc) / width;
					if (L > 254)
						L = 254;
					Scalar c = ScalarHSV2BGR(300, S, L);
					Vec3b val;
					val[0] = (int)c[0];
					val[1] = (int)c[1];
					val[2] = (int)c[2];
					//val[0] = 0; val[1] = (i * 255) / height; val[2] = (height - i) * 255 / height;

					image.at<Vec3b>(i, x) = val;
				}
			}
			else if (render == 7)
			{
				for (int x = 0; x < width; x++)
				{
					S = 254;
					SL_inc = 2.0 * SL_MAX / height;
					if(i<height/2)
					{ 
						if (x < width / 2)
							L = SL_MAX - (i*SL_inc + 2.0*x*(SL_MAX - i*SL_inc) / width);
						else
							L = SL_MAX - (SL_MAX - 2.0*(x - width / 2)*(SL_MAX - i*SL_inc) / width);// (i*SL_inc + SL_MAX - i*SL_inc - 2.0*(x - width / 2) *(SL_MAX - i*SL_inc) / width);
					}
					else
					{
						if (x<width / 2)
							L = SL_MAX - (SL_MAX-(i- height/ 2)  *SL_inc + 2.0*x*(SL_MAX - SL_MAX + (i - height / 2)  *SL_inc) / width);
						else
							L = SL_MAX - (SL_MAX - 2.0*(x - width / 2)*(SL_MAX - SL_MAX + (i - height / 2)  *SL_inc) / width);
					}
					
					//L = i*SL_inc + x*(SL_MAX - i*SL_inc) / width;
					if (L > 254)
						L = 254;
					Scalar c = ScalarHSV2BGR(300, S, L);
					Vec3b val;
					val[0] = (int)c[0];
					val[1] = (int)c[1];
					val[2] = (int)c[2];
					//val[0] = 0; val[1] = (i * 255) / height; val[2] = (height - i) * 255 / height;

					image.at<Vec3b>(i, x) = val;
				}

			}
			else if (render == 8)
			{
				image.copyTo(background);
				Scalar c = ScalarHSV2BGR(300, 200, 200);
				line(background, pt1, pt2, c, 1, 0);
				addWeighted(background, alpha_bkg, image, 1 - alpha_bkg, 0, syn);
				syn.copyTo(image);
				alpha_bkg -= alpha_inc;
				if (alpha_bkg < 0)
					alpha_bkg = 0;
			}
			else if (render == 9)
			{
				Vec3b val;
				val[0] = 0; val[1] = (i * 255) / height; val[2] = (height - i) * 255 / height;
				for (int x = 0; x < width; x++)
					image.at<Vec3b>(i, x) = val;
			}
			
		}
		image.copyTo(background);

		if (defect_type == 0)//rectangle
		{
			Point pt1, pt2;
			pt1.x = width / 2 - rng.uniform(0.0, def_xran/10);
			pt1.y = height / 2 - rng.uniform(0.0, def_yran / 10);
			pt2.x = pt1.x + rng.uniform(0.0, def_xran);
			pt2.y = pt1.y + rng.uniform(0.0, def_yran);
			rectangle(image, pt1, pt2, Scalar(128, 128, 128), defect_level, lineType);
			box_x = pt1.x;
			box_y = pt1.y;
			box_w = pt2.x - pt1.x;
			box_h = pt2.y - pt1.y;
		}
		else if (defect_type == 1)//rectangle
		{
			Point pt1, pt2;
			pt1.x = width / 2 - rng.uniform(0.0, def_xran / 10);
			pt1.y = height / 2 - rng.uniform(0.0, def_yran / 10);
			pt2.x = pt1.x + rng.uniform(0.0, def_xran);
			pt2.y = pt1.y + rng.uniform(0.0, def_yran);
			rectangle(image, pt1, pt2, Scalar(128, 128, 128), -1, lineType);
			box_x = pt1.x;
			box_y = pt1.y;
			box_w = pt2.x - pt1.x;
			box_h = pt2.y - pt1.y;
		}
		else if(defect_type == 2)//triangle
		{ 
			std::vector<std::vector<Point> > data(1);
			def_x = rng.uniform(width / 2- def_xran/2, width / 2 + def_xran / 2);
			def_y = rng.uniform(height / 2 - def_yran / 2, height / 2 + def_yran / 2);
			data[0].push_back(Point(def_x, def_y));
			def_x = rng.uniform(width / 2 - def_xran / 2, width / 2 + def_xran / 2);
			def_y = rng.uniform(height / 2 - def_yran / 2, height / 2 + def_yran / 2);
			data[0].push_back(Point(def_x, def_y));
			def_x = rng.uniform(width / 2 - def_xran / 2, width / 2 + def_xran / 2);
			def_y = rng.uniform(height / 2 - def_yran / 2, height / 2 + def_yran / 2);
			data[0].push_back(Point(def_x, def_y));
			drawContours(image, data, -1, Scalar(128, 128, 128), defect_level, lineType);
			int minx, miny, maxx, maxy;
			minx = data[0][0].x;
			maxx = data[0][0].x;
			miny = data[0][0].y;
			maxy = data[0][0].y;
			for (int l = 0; l < data[0].size(); l++)
			{
				if (data[0][l].x < minx)
					minx = data[0][l].x;
				if (data[0][l].x > maxx)
					maxx = data[0][l].x;
				if (data[0][l].y < miny)
					miny = data[0][l].y;
				if (data[0][l].y > maxy)
					maxy = data[0][l].y;
			}
			box_x = minx;
			box_y = miny;
			box_w = maxx - minx;
			box_h = maxy - miny;
		}
		else if (defect_type == 3)//triangle
		{
			std::vector<std::vector<Point> > data(1);
			def_x = rng.uniform(width / 2 - def_xran / 2, width / 2 + def_xran / 2);
			def_y = rng.uniform(height / 2 - def_yran / 2, height / 2 + def_yran / 2);
			data[0].push_back(Point(def_x, def_y));
			def_x = rng.uniform(width / 2 - def_xran / 2, width / 2 + def_xran / 2);
			def_y = rng.uniform(height / 2 - def_yran / 2, height / 2 + def_yran / 2);
			data[0].push_back(Point(def_x, def_y));
			def_x = rng.uniform(width / 2 - def_xran / 2, width / 2 + def_xran / 2);
			def_y = rng.uniform(height / 2 - def_yran / 2, height / 2 + def_yran / 2);
			data[0].push_back(Point(def_x, def_y));
			drawContours(image, data, -1, Scalar(128, 128, 128), -1, lineType);
			int minx, miny, maxx, maxy;
			minx = data[0][0].x;
			maxx = data[0][0].x;
			miny = data[0][0].y;
			maxy = data[0][0].y;
			for (int l = 0; l < data[0].size(); l++)
			{
				if (data[0][l].x < minx)
					minx = data[0][l].x;
				if (data[0][l].x > maxx)
					maxx = data[0][l].x;
				if (data[0][l].y < miny)
					miny = data[0][l].y;
				if (data[0][l].y > maxy)
					maxy = data[0][l].y;
			}
			box_x = minx;
			box_y = miny;
			box_w = maxx - minx;
			box_h = maxy - miny;
		}
		else if (defect_type == 4)//line
		{
			Point pt1, pt2;
			int minx, miny, maxx, maxy;
			pt1.x = rng.uniform(width / 2 - def_xran / 2, width / 2 + def_xran / 2);
			pt1.y = rng.uniform(height / 2 - def_yran / 2, height / 2 + def_yran / 2);
			minx = pt1.x;
			maxx = pt1.x;
			miny = pt1.y;
			maxy = pt1.y;
			for (int k = 0; k < 4; k++)
			{
				pt2.x = rng.uniform(width / 2 - def_xran / 2, width / 2 + def_xran / 2);
				pt2.y = rng.uniform(height / 2 - def_yran / 2, height / 2 + def_yran / 2);
				line(image, pt1, pt2, Scalar(128, 128, 128), defect_level, lineType);
				pt1.x = pt2.x;
				pt1.y = pt2.y;
				if (pt2.x < minx)
					minx = pt2.x;
				if (pt2.x > maxx)
					maxx = pt2.x;
				if (pt2.y < miny)
					miny = pt2.y;
				if (pt2.y > maxy)
					maxy = pt2.y;
			}

			box_x = minx;
			box_y = miny;
			box_w = maxx - minx;
			box_h = maxy - miny;
		}
		else //curve
		{
			std::vector<Point> points;
			for(int k=0;k<16;k++)
			{ 
				def_x = rng.uniform(0.0, def_xran); //0;// rng.uniform(double(width / 2 - 20), double(width / 2 + 20));
				def_y = rng.uniform(0.0, def_yran); //0;// rng.uniform(double(height / 2 - 20), double(height / 2 + 20));
				points.push_back(Point(def_x, def_y));
			} 
			Mat A;
			polynomial_curve_fit(points, 3, A);
			std::vector<Point> points_fitted;
			for (int x = 0; x < 400; x++)
			{
				double y = A.at<double>(0, 0) + A.at<double>(1, 0) * x +
					A.at<double>(2, 0)*std::pow(x, 2) + A.at<double>(3, 0)*std::pow(x, 3);
				if (x < def_xran && y < def_xran && x>0 && y>0)
					points_fitted.push_back(Point(x + width / 2 - def_xran / 2, y + height / 2 - def_yran / 2));
			}

			polylines(image, points_fitted, false, Scalar(128, 128, 128), defect_level, lineType, 0);
			int minx, miny, maxx, maxy;
			minx = points_fitted[0].x;
			maxx = points_fitted[0].x;
			miny = points_fitted[0].y;
			maxy = points_fitted[0].y;
			for (int l = 0; l < points_fitted.size(); l++)
			{
				if (points_fitted[l].x < minx)
					minx = points_fitted[l].x;
				if (points_fitted[l].x > maxx)
					maxx = points_fitted[l].x;
				if (points_fitted[l].y < miny)
					miny = points_fitted[l].y;
				if (points_fitted[l].y > maxy)
					maxy = points_fitted[l].y;
			}
			box_x = minx;
			box_y = miny;
			box_w = maxx - minx;
			box_h = maxy - miny;
			
		}
		if (set_transavg >= 0 && set_transvar >= 0 && set_transavg - set_transvar >= 0&& set_transavg + set_transvar<=100)
			alpha = (double)(rng.gaussian(set_transvar) + set_transavg)/100.0;
		else
			alpha = rng.uniform(0.1, 0.5);
		//alpha = 0.5;
		addWeighted(image, alpha, background, 1 - alpha, 0, syn);
		Point pt1, pt2;
		pt1.x = box_x;
		pt1.y = box_y;
		pt2.x = box_x+box_w;
		pt2.y = box_y+box_h;
		//rectangle(syn, pt1, pt2, Scalar(0, 0, 255), 1, lineType);
		imshow(wndname, syn);
		if (waitKey(DELAY) >= 0)
			return 0;
		sprintf_s(imgfname, "D_%05d.png", j);
		imwrite(imgfname, syn);
		fout << imgfname<<", ";
		switch (defect_type)
		{
		case 0:
			fout << "Rectangle_Hole, ";
			break;
		case 1:
			fout << "Rectangle_Fill, ";
			break;
		case 2:
			fout << "Triangle_Hole, ";
			break;
		case 3:
			fout << "Triangle_Fill, ";
			break;
		case 4:
			fout << "Line, ";
			break;
		default:
			fout << "Curve, ";
			break;
		}
		fout << box_x << ", "<<box_y<< ", "<<box_w <<", "<<box_h<<std::endl;
		//imwrite("OpenCV_Syn.jpg", syn);
	}
	if (waitKey(10000) >= 0)
		return 0;
}